﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System.Linq;

public class fbManager : MonoBehaviour
{
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init();
        }
        else
        {
            FB.ActivateApp();
        }
    }

    public void Share()
    {
        if(!FB.IsLoggedIn)
        {
            FB.LogInWithReadPermissions(null, callback: OnLogin);
        }
        else
        {
            FB.ShareLink(contentTitle: "Testing", contentURL: new System.Uri("https://aleandrovalencia.github.io"), contentDescription: "Like and Share my game", callback: OnShare);
        }
    }

    private void OnLogin(ILoginResult result)
    {
        if(result.Cancelled)
        {
            Debug.Log("user cancelled login");
        }
        else
        {
            Share();
        }
    }

    private void OnShare(IShareResult result)
    {
        if(result.Cancelled || !string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("sharelink error: " + result.Error);
        }
        else if(!string.IsNullOrEmpty(result.PostId))
        {
            Debug.Log("link shared");
        }
    }
}
